import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import AdminPage from "../views/AdminPage";
import EditUser from "../views/EditUser";

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'main',
    component: AdminPage
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/edit-user/:login/:state?',
    name: 'edit-user',
    component: EditUser,
  },
  {
    path: '/users',
    name: 'users',
    component: AdminPage,
    props: () => ({
        isAdmin:
          localStorage.getItem('login')
          && localStorage.getItem('role') === 'Administrators'
      }
    ),
    beforeEnter: (from, to, next) => {
      if (localStorage.getItem('login')) {
        next();
      } else {
        next({
          name: 'login',
          params: {outerMsg: 'YOU ARE NOT LOGGED IN.'}
        });
      }
    },
  },
];

const router = new VueRouter({
  routes
});

export default router
