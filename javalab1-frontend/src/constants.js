export const backendBaseUrl = "http://localhost:8000";
export const backendUrls = {
  login: backendBaseUrl + "/api/login",
  logout: backendBaseUrl + "/api/logout",
  user: backendBaseUrl + "/api/",
};