from starlette.requests import Request
import logging

logger = logging.getLogger(__name__)


async def logging_middleware(request: Request, call_next):
    logger.info("started request")
    logging.debug("request body %s", await request.body() and await request.json())
    logging.debug("request query params %s", request.query_params)
    response = await call_next(request)
    logger.info("ended request")
    return response
