from starlette.testclient import TestClient

from app.app import app
from app.init_data import populate_default_data, ensure_tables_exist

client = TestClient(app)


def test_read_main():
    ensure_tables_exist()
    populate_default_data()
    response = client.get("/api")
    assert response.status_code == 200
    assert response.json()[0]['login'] == 'admin'
