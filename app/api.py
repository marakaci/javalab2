import uuid

from fastapi import APIRouter, Cookie
from starlette.responses import Response, JSONResponse
from starlette.status import HTTP_422_UNPROCESSABLE_ENTITY, HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED

from app import user_repository
from app.common_utils import cookie_storage
from app.models import UserLogin, User

router = APIRouter()


def is_session_cookie_valid(session_cookie: str):
    try:
        login, session_id = session_cookie.rsplit('|', maxsplit=1)
        return login in cookie_storage and cookie_storage[login] == session_id
    except (ValueError, AttributeError):
        return False


@router.post('/login')
def user_login(login_model: UserLogin):
    users = user_repository.get_anybody_by_login(login_model.login)
    if len(users) < 1:
        return Response("Wrong login or password", status_code=HTTP_400_BAD_REQUEST)

    user = users[0]

    if user.password != login_model.password:
        return Response("Wrong login or password", status_code=HTTP_400_BAD_REQUEST)

    session_id = uuid.uuid4().hex
    cookie_storage[login_model.login] = session_id
    session_cookie = f'{login_model.login}|{session_id}'

    response = JSONResponse(dict(login=login_model.login, role=user.group_name))
    response.set_cookie('session_cookie', session_cookie)

    return response


@router.post('/logout')
def user_logout(session_cookie=Cookie("")):
    if is_session_cookie_valid(session_cookie):
        login, session_id = session_cookie.rsplit('|', maxsplit=1)
        del cookie_storage[login]


@router.get('/')
def get_user_by_login(login: str = None, session_cookie=Cookie("")):
    if login is None:
        users = user_repository.get_everybody()

        return users
    user = user_repository.get_anybody_by_login(login)[0]

    return user
    # return SimpleUser(**user.dict())

# TODO - add validation
@router.post('/')
def add_user(user: User, session_cookie=Cookie("")):
    if not is_session_cookie_valid(session_cookie):
        return Response(status_code=HTTP_401_UNAUTHORIZED)

    login = user.login
    if len(user_repository.get_anybody_by_login(login)) > 0:
        return Response(f"User with login {login} already exists!", status_code=HTTP_422_UNPROCESSABLE_ENTITY)
    try:
        return user_repository.add_anybody(user)
    except RuntimeError:
        return Response("Failed to add user, probably foreign key is invalid.", status_code=HTTP_400_BAD_REQUEST)


@router.put('/')
def edit_user(login: str, user: User, session_cookie=Cookie("")):
    if not is_session_cookie_valid(session_cookie):
        return Response(status_code=HTTP_401_UNAUTHORIZED)

    return user_repository.edit_anybody(login, user)


@router.delete('/')
def delete_user(login: str, session_cookie=Cookie("")):
    if not is_session_cookie_valid(session_cookie):
        return Response(status_code=HTTP_401_UNAUTHORIZED)

    user_repository.delete_anybody(login)
