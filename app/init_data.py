import logging

from app.common_utils import table_exists, sql_nonquery, sql_query

logger = logging.getLogger(__name__)


def ensure_tables_exist():
    logging.info("Checking if tables exist...")
    if not table_exists("GROUPS"):
        logging.info("TABLE GROUPS DOESN'T EXIST, CREATING...")
        sql_nonquery("""
           CREATE TABLE GROUPS (
               name TEXT PRIMARY KEY ,
               description TEXT default ''
           )
           """)
    if not table_exists("USERS"):
        logging.info("TABLE USERS DOESN'T EXIST, CREATING...")
        sql_nonquery("""
           CREATE TABLE USERS (
               login TEXT PRIMARY KEY ,
               full_name TEXT NOT NULL ,
               password TEXT NOT NULL ,
               group_name TEXT NOT NULL ,
               FOREIGN KEY (group_name) REFERENCES GROUPS(name)
           )
           """)
    logging.info("ALl tables exist.")


def populate_default_data():
    admin_groups = sql_query("""SELECT name FROM GROUPS WHERE name = 'Administrators';""")
    if len(admin_groups) < 1:
        logging.info("Administrators group doesn't exist, creating...")
        sql_nonquery("""INSERT INTO GROUPS VALUES ('Administrators', 'Admin group.')""")

    admins = sql_query("""SELECT login FROM USERS WHERE group_name = 'Administrators';""")
    if len(admins) < 1:
        logging.info("Default admin doesn't exist, creating...")
        sql_nonquery("""
        INSERT INTO USERS 
        VALUES ('admin', 'Administrator of the system', 'admin', 'Administrators')
        """)

    user_groups = ['TeamA', 'TeamB', 'TeamC']
    default_users = [
        ('ivanov', 'Ivanov Ivan Ivanovich', 'password', 'TeamA'),
        ('petrov', 'Petrov Petr Petrovich', 'password', 'TeamB'),
        ('sidorov', 'Sidorov Sidor Sidorovich', 'password', 'TeamC'),
    ]
    for group, default_user in zip(user_groups, default_users):
        user_group = sql_query("""SELECT name from GROUPS WHERE name = ?""", group)
        if len(user_group) < 1:
            logging.info(f"User group {group} doesn't exist, creating...")
            sql_nonquery("""INSERT INTO GROUPS VALUES (?, ?)""", group, f"User group {group}.")

        users = sql_query("""SELECT login FROM USERS WHERE group_name = ?""", group)
        if len(users) < 1:
            logging.info(f"Group {group} is emtpy, creating default values...")
            sql_nonquery("""INSERT INTO USERS VALUES (?, ?, ?, ?)""", *default_user)
    logging.info('Data is present.')
