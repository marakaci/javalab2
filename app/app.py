from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from app import api
from app.middlewares import logging_middleware

origins = [
    "http://localhost:8080",
]

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.middleware("http")(logging_middleware)


app.include_router(
    api.router,
    prefix='/api',
    tags=['User actions']
)