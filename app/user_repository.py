import sqlite3
from typing import List

from app.common_utils import sql_query, sql_nonquery
from app.models import User


def users_from_rows(rows: tuple) -> List[User]:
    res = []
    for row in rows:
        login, full_name, password, group_name = row
        user = User(login=login, full_name=full_name, password=password, group_name=group_name)
        res.append(user)
    return res


def get_user_by_login(login: str):
    return users_from_rows(
        sql_query(
            """SELECT * FROM USERS WHERE login = ? AND group_name != 'Administrators'""",
            login
        )
    )


def get_users():
    return users_from_rows(
        sql_query(
            """SELECT * FROM USERS WHERE group_name != 'Administrators'"""
        )
    )


def get_admin_by_login(login: str):
    return users_from_rows(
        sql_query(
            """SELECT * FROM USERS WHERE login = ? AND group_name = 'Administrators'""",
            login
        )
    )


def get_admins():
    return users_from_rows(
        sql_query(
            """SELECT * FROM USERS WHERE group_name = 'Administrators'"""
        )
    )


def get_everybody():
    return users_from_rows(
        sql_query(
            """SELECT * FROM USERS;"""
        )
    )


def get_anybody_by_login(login: str):
    return users_from_rows(
        sql_query(
            """SELECT * FROM USERS WHERE login = ?""",
            login
        )
    )


def add_anybody(user: User):
    try:
        sql_nonquery("""INSERT INTO USERS VALUES (?, ?, ?, ?)""",
                     user.login,
                     user.full_name,
                     user.password,
                     user.group_name)
    except sqlite3.IntegrityError as e:
        raise RuntimeError(str(e))

    return user.login


def edit_anybody(login: str, user: User):
    sql_nonquery("""UPDATE USERS SET full_name = ?, password = ?, group_name = ? WHERE login = ?""",
                 user.full_name,
                 user.password,
                 user.group_name,
                 login)

    # in case somebody tries to change login
    # we don't
    user.login = login

    return user


def delete_anybody(login: str):
    sql_nonquery("""DELETE FROM USERS WHERE login = ?""", login)
