from pydantic import BaseModel, validator


class User(BaseModel):
    login: str
    full_name: str
    password: str
    group_name: str

    @validator('login')
    def login_min_5_chars(cls, v):
        if len(v) < 5:
            raise ValueError("Login length must be 5+ chars.")

        return v

    @validator('password')
    def password_min_3_chars(cls, v):
        if len(v) < 3:
            raise ValueError("Password length must be 3+ chars.")

        return v

    @validator('full_name')
    def full_name_not_empty(cls, v):
        if len(v) < 1:
            raise ValueError("Full Name must be NOT empty.")

        return v


class SimpleUser(BaseModel):
    login: str
    full_name: str
    group_name: str


class UserLogin(BaseModel):
    login: str
    password: str
