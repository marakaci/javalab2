import sqlite3

cookie_storage = {}

DB_CONNECTION_STRING = 'javalab1.db'


def table_exists(table_name: str):
    tables = sql_query("""SELECT name FROM sqlite_master WHERE type='table' AND name=?;""", table_name)

    return len(tables) > 0


def sql_query(query: str, *parameters):
    connection = sqlite3.connect(DB_CONNECTION_STRING)
    cursor = connection.cursor()
    cursor.execute(query, parameters)

    return cursor.fetchall()


def sql_nonquery(query: str, *parameters):
    connection = sqlite3.connect(DB_CONNECTION_STRING)
    connection.execute("PRAGMA foreign_keys = ON")
    cursor = connection.cursor()
    cursor.execute(query, parameters)

    connection.commit()
