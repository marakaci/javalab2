from logging.config import fileConfig
fileConfig('logger_config.ini')

from app.init_data import ensure_tables_exist, populate_default_data

import uvicorn

if __name__ == "__main__":
    ensure_tables_exist()
    populate_default_data()
    uvicorn.run("app.app:app", host="0.0.0.0", port=8000, reload=True)
