from setuptools import setup, find_packages
from os.path import splitext, basename
import glob

install_reqs = [req.strip() for req in open('requirements.txt').readlines()]

setup(
    name="JavaLab2",
    version="0.0.2",
    packages=find_packages(),
    install_requires=install_reqs,
    url="https://gitlab.com/marakaci/javalab2",
    py_modules=[splitext(basename(path))[0] for path in glob.glob('app/*.py')]
)
